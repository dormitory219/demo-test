//
//  IOSModuleInitialize.h
//  Demo
//
//  Created by 罗贤明 on 2018/5/12.
//  Copyright © 2018年 罗贤明. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Axe.h"

/**
 模块注册。
 */
@interface IOSModuleInitialize : NSObject<AXEModuleInitializer>

@end
