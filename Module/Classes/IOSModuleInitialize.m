//
//  IOSModuleInitialize.m
//  Demo
//
//  Created by 罗贤明 on 2018/5/12.
//  Copyright © 2018年 罗贤明. All rights reserved.
//

#import "IOSModuleInitialize.h"
#import "DemoGround.h"
#import "TestViewController.h"

@implementation IOSModuleInitialize
AXEMODULEINITIALIZE_REGISTER()
- (void)AEXInitialize {
    [AXEAutoreleaseEvent registerSyncListenerForEventName:AXEEventModulesBeginInitializing handler:^(AXEData *payload) {
        [[AXERouter sharedRouter] registerPath:@"ios/test" withViewRoute:^UIViewController *(AXERouteRequest *request) {
            return [[TestViewController alloc] init];
        }];
    } priority:DEMOGROUND_MODULE_INIT_PRIORITY];
}

@end
